//
//  TableViewDataSource.swift
//  iFlickr
//
//  Created by Vitaly Malkov on 22.09.2021.
//

import UIKit

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listPhoto?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as! ImageCell
        let photo = listPhoto?.items[indexPath.row]
        cell.nameLabel.text = photo?.title ?? "No name"
        cell.tagsLabel.text = photo?.tags
        cell.dateLabel.text = dateDecoder(date: photo?.date_taken)
        cell.imgView?.loadImageUsingCache(withUrl: photo?.media.m ?? "")
        
        tableView.hideActivityIndicator()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let currentCell = tableView.cellForRow(at: indexPath) as! ImageCell
        
        imageToPass = currentCell.imgView?.image
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else { return }
        vc.selectedImage = imageToPass
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
