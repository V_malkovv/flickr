//
//  SearchBarDelegate.swift
//  iFlickr
//
//  Created by Vitaly Malkov on 23.09.2021.
//

import UIKit

extension MainViewController: UISearchResultsUpdating, UISearchControllerDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        if let searchTimer = searchTimer {
            searchTimer.invalidate()
        }
        searchTimer = Timer.scheduledTimer(timeInterval: 0.75, target: self, selector: #selector(self.doMyFilter), userInfo: nil, repeats: false)
    }
}
