//
//  NetworkService.swift
//  iFlickr
//
//  Created by Vitaly Malkov on 24.09.2021.
//

import UIKit

class NetworkService {

    func request(_ tag: String, urlString: String, completion: @escaping (Result<ListPhoto, Error>) -> Void) {
        let urlString = urlString + tag
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    print("Some error")
                    completion(.failure(error))
                    return
                }
                guard let data = data else { return }
                do {
                    let photos = try JSONDecoder().decode(ListPhoto.self, from: data)
                    completion(.success(photos))
                } catch let jsonError {
                    print("Failed to decode JSON", jsonError)
                    completion(.failure(jsonError))

                }

            }

        }.resume()
        
    }

    private func getPhoto(_ url: String) -> UIImage {
        let url = URL(string: url)
        var image = UIImage()

        DispatchQueue.global().async {
            guard let url = url else { return }
            let data = try? Data(contentsOf: url)
            DispatchQueue.main.async {
                guard let data = data else { return }
                image = UIImage(data: data) ?? UIImage()
            }
        }
        return image
    }

}
