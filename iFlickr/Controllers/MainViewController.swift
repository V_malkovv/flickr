//
//  MainViewController.swift
//  iFlickr
//
//  Created by Vitaly Malkov on 22.09.2021.
//

import UIKit

class MainViewController: UIViewController, XMLParserDelegate, UISearchBarDelegate {
    
    // MARK: - Constants
    
    private let urlString = "https://www.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1&tags="
    private let networkService = NetworkService()
    
    // MARK: - Properties
    
    var listPhoto: ListPhoto? = nil
    var imageToPass: UIImage?
    var tag = ""
    
    var searchTimer: Timer?
    
    private var pullControl = UIRefreshControl()
    
    private lazy var searchController: UISearchController = {
        let sc = UISearchController(searchResultsController: nil)
        sc.searchResultsUpdater = self
        sc.delegate = self
        sc.obscuresBackgroundDuringPresentation = false
        sc.searchBar.placeholder = "Search photo"
        sc.searchBar.searchTextField.backgroundColor = UIColor.white
        return sc
    }()
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet private weak var segmentControl: UISegmentedControl!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        setupNavigationBar()
        setupRefreshControl()
        
        tableView.showActivityIndicator()
        
        getData()
    }
    
    // MARK: - IBActions
    
    @IBAction private func sortingSegmentControl(_ sender: UISegmentedControl) {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            self.listPhoto?.items.sort(by: { $0.title < $1.title } )
        case 1:
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            self.listPhoto?.items.sort(by: {
                df.date(from: $0.date_taken)!.compare( df.date(from: $1.date_taken)!) == .orderedDescending
            })
        default:
            break
        }
        tableView.reloadData()
        
    }
    
    // MARK: - Private Methods
    
    private func setupNavigationBar() {
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    private func configureView() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func setupRefreshControl() {
        pullControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        pullControl.addTarget(self, action: #selector(refreshListData(_:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = pullControl
        } else {
            tableView.addSubview(pullControl)
        }
        
    }
    
    // MARK: - Internal Methods
    
    func dateDecoder(date: (String?)) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: date!)!
        dateFormatter.dateFormat = "MMM d, yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "HST")
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    func getData() {
        networkService.request(tag, urlString: urlString) { [weak self] (result) in
            switch result {
            case .success(let listPhoto):
                self?.listPhoto = listPhoto
                self?.tableView.reloadData()
            case .failure(let error):
                print("error:", error)
            }
            
        }
        
    }
    
    @objc func doMyFilter() {
        tag = searchController.searchBar.text ?? ""
        getData()
    }
    
    @objc private func refreshListData(_ sender: Any) {
        self.pullControl.endRefreshing()
        getData()
    }
    
}
