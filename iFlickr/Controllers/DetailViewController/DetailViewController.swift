//
//  DetailViewController.swift
//  iFlickr
//
//  Created by Vitaly Malkov on 22.09.2021.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var detailImageView: UIImageView!
    
    var selectedImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailImageView.image = selectedImage
    }
    
}
