//
//  TableViewCell.swift
//  iFlickr
//
//  Created by Vitaly Malkov on 23.09.2021.
//

import UIKit

class ImageCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
