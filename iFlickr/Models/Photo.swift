//
//  Photo.swift
//  iFlickr
//
//  Created by Vitaly Malkov on 23.09.2021.
//

import UIKit

struct ListPhoto: Decodable {
    var items: [Photo]
}

struct Photo: Decodable {
    var title: String
    var media: Media
    var date_taken: String
    var tags: String?
    
}

struct Media: Decodable {
    var m: String
}


